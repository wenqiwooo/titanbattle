# TITAN BATTLE #

## Setting ##
You are in a dystopic world. The world is dying. The last remaining four factions of human race are engaging each other in meaningless battles over this artifact called a warp gem, which is actually a portal to a star system with life-sustaining planets. Help your faction gain possession of the warp gem and give your people a new change at life.
In this world, human engage battles controlling large humanoid machines called Titans. 

## Game Rules ##
Each team will consist of three players, each of whom will control a Titan. You can choose any of the following classes of Titans to use:

### Alpha ###
Alpha-class Titans are known for their overall competence in battle. Equipped with advanced weapons, decent speed and armour, they can turn the tide of the meanest battles. While they may not be as fast as Beta-class Titans and as fortified as Omega-class Titans, they are the most well-rounded and have few weaknesses.

Ease of play: Easy
Weapon: Hammer
Health: Can take 2 Hammer hits, 3 Grenade hits, 4 Snipes (for weapon base config)


### Beta ###
Beta-class Titans are tanks. Though slow moving and bulky, the thick armour plates worn by these Titans have proven to be impenetrable again and again. Equipped with an AoE EMP grenade that turns other Titans into useless piles of metal junk, this Titan is a beast in the battlefield. However due to the thick armour, Beta-class Titans are resistant to each other’s EMP grenades.

Ease of play: Medium
Weapon: Grenade
Health: Can take 3 Hammer hits, 6 Snipes, immune to Grenades (for weapon base config)


### Omega ###
Omega-class Titans are light-weighted and fast. To achieve such light weight, these Titans have to sacrifice huge amounts of armour. However, with the sniper-rifles equipped by these machines that supersedes the ranges of other Titans’ weapons, there is seldom a situation where you will find a Omega-class Titan within firing range of another Titan. Use this Titan strategically and your opponent will never know what’s coming.

Ease of play: Difficult
Health: Can take 1 hammer hit, 1 grenade hit, 2 snipe (for weapon base config)

As a player, you will have to create a class that inherits any of the Titan-class interface, i.e. Alpha, Beta and Omega.


## How the game teaches OOP ##
The game can provide students the following series of tasks to teach them Object-oriented programming.

### Configure your Titan’s inventory ###
Every Titan has a ConfigList object that tells the system what items they would like to bring with them into the game

### Equipping your Titan and attacking ###
Players can equip their Titan object with the Weapon object they created previously and use it to attack.

### Controlling the movement of your Titan ###
Players can adopt various algorithms to maneuver their Titan in the battlefield to specific objectives.

### Moving as a team ###
Players can have their Titans communicate with other Titans in the team via a Communicator. A Communicator object contains a flag for each Titan which that Titan can assign a value to and other Titans in the team can read from. This facilitates coordination of movement and employment of strategies.

### Attacking as a team against other Titans ###
This is the finale and perhaps the most challenging aspect of the game. Players will have to coordinate their attacks to fend off enemy Titans. Because each Titan class has its unique weapon capabilities and attributes, players will have to strategize their attack to engage the enemy effectively. For example, an Alpha-class Titan engaging an enemy who is holding the warp gem, while a Omega-class Titan snipes the enemy reinforcements while taking cover behind an Beta-class Titan.

## Things to implement in summer ##
After the warp gem has been captured by a Titan, he has to communicate with the Ship to land on the edge of the battlefield to pick him up.

Dropping of the warp gem when a player dies.