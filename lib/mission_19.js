// Auto-Generated
makeDropZone(document.querySelector('body'), function (data) {
  console.log(parse_and_evaluate(data));
});

function loaded() {
    export_symbol('test_task', test_task);
    export_symbol('is_instance_of', is_instance_of);
    export_symbol('NamedObject', NamedObject);
    export_symbol('Room', Room);
    export_symbol('ProtectedRoom', ProtectedRoom);
    export_symbol('Ship', Ship);
    export_symbol('MobileObject', MobileObject);
    export_symbol('Generator', Generator);
    export_symbol('LivingThing', LivingThing);
    export_symbol('Person', Person);
    export_symbol('Player', Player);

    // Titans
    export_symbol('ALPHAPlayer', ALPHAPlayer);
    export_symbol('BETAPlayer', BETAPlayer);
    export_symbol('OMEGAPlayer', OMEGAPlayer);
    export_symbol('ServiceBot', ServiceBot);
    export_symbol('SecurityDrone', SecurityDrone);

    // AI Titans
    export_symbol('TitanBot', TitanBot);

    export_symbol('Thing', Thing);
    // Transporters
    export_symbol('Transporter', Transporter);
    export_symbol('ALPHATransporter', ALPHATransporter);
    export_symbol('BETATransporter', BETATransporter);
    export_symbol('OMEGATransporter', OMEGATransporter);

    export_symbol('Weapon', Weapon);
    export_symbol('MeleeWeapon', MeleeWeapon);
    export_symbol('RangedWeapon', RangedWeapon);
    export_symbol('SpellWeapon', SpellWeapon);
    export_symbol('Bomb', Bomb);
    export_symbol('Keycard', Keycard);
}

window.onload = loaded;