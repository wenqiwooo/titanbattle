function is_null(xs) {
	return xs === null;
}

function is_number(xs) {
	return typeof xs === "number";
}

function is_string(xs) {
	return typeof xs === "string";
}

function is_boolean(xs) {
	return typeof xs === "boolean";
}

function is_object(xs) {
	return typeof xs === "object" || is_function(xs);
}

function is_function(xs) {
	return typeof xs === "function";
}

function is_NaN(x) {
	return is_number(x) && isNaN(x);
}

function has_own_property(obj,p) {
	return obj.hasOwnProperty(p);
}

function is_array(a){
	return a instanceof Array; 
}

/**
 * @deprecated Use timed instead.
 * @returns The current time, in milliseconds, from the Unix Epoch.
 */
function runtime() {
	var d = new Date();
	return d.getTime();
}

/**
 * Throws an error from the interpreter, stopping execution.
 *
 * @param {string} message The error message.
 * @param {number=} line The line number where the error occurred. This line number
 *                       will be one less than on file, because the indices used by
 *                       jison start from 0.
 * @returns {null} Should not return. Exception should be thrown otherwise program
 *                 will be in an invalid state.
 */
function error(message, line) {
	throw new SyntaxError(message, null,
		line === undefined ? undefined : line + 1);
}

function newline(){
	display("\n");
}

function display(str){
	var to_show = str;
	if (is_array(str) && is_empty_list(str)) {
		to_show = '[]';
	} else if (is_pair(str)) {
		to_show = '';
		var stringize = function(item) {
			if (is_empty_list(item)) {
				return '[]';
			} else if (is_pair(item)) {
				return '[' + stringize(head(item)) + ', ' + stringize(tail(item)) + ']';
			} else {
				return item.toString();
			}
		}
		
		to_show = stringize(str);
	}
	//process.stdout.write(to_show);
	console.log(to_show);
	return str;
}

function random(k){
	return Math.floor(Math.random()*k);
}

function timed(f) {
	var self = this;
	var timerType = window.performance ? performance : Date;
	return function() {
		var start = timerType.now();
		var result = f.apply(self, arguments);
		var diff = (timerType.now() - start);
		console.log('Duration: ' + Math.round(diff) + 'ms');
		return result;
	};
}

(function() {
	var commentRegex = /(\/\/[^\n\r$]*[\n\r$])|(\/\*[^$]*?(\*[^$]+\/)*?[^$]*?\*\/)/g;
	var nativeFuncRegex = /^(function\s+)(\S*)(\s*\([\w\s,]*\))(\s*\{[^$]*\})$/m;
	var nativeRef = 'setTimeout', nativeCodeParts = window[nativeRef].toString().replace(commentRegex, '').match(nativeFuncRegex);
	
	// For compatbility with Non-Gecko and WebKit browsers:
	// Function.prototype.name: inspired by http://stackoverflow.com/a/17056530
	if (!Function.hasOwnProperty('name')) {
		Object.defineProperty(Function.prototype, 'name', {
			get: function() {
				var name = this.toString().match(nativeFuncRegex)[2];
				// For better performance only parse once, and then cache the
				// result through a new accessor for repeated access.
				Object.defineProperty(this, 'name', {value: name});
				return name;
			}
		});
	}
	
	// OBFUSCATE STARTS HERE
	function obfuscate(func, name, params, hideParameters) {
		var codeParts = func.toString().replace(commentRegex, '').match(/^\(?function(?:\s+\S*|)\s*(\([\w\s,]*\))/m);
		var codeStr = nativeCodeParts[1] + (name || func.name) + (hideParameters ? nativeCodeParts[3] : (params || codeParts[1])) + nativeCodeParts[4];
		func.toString = func.toSource = func.toLocaleString = (function(source) {return function() {return source;};})(codeStr);
		return func;
	}
	Object.defineProperty(window, 'obfuscate', {value: obfuscate(obfuscate, 'obfuscate', true)});
})();

function read(x) {
	return prompt(x);
}

function write(x) {
	return alert(x);
}

function parse(x) {
	if (x === null) {
		return {"tag": "exit"};
	} else {
		return parser.parse(x);
	}
}

function apply_in_underlying_javascript(prim,argument_list) {
   var argument_array = list_to_vector(argument_list);

   //Call prim with the same this argument as what we are running under.
   //this is populated with an object reference when we are an object. We
   //are not in this context, so this will usually be the window. Thus
   //passing this as the argument shouls behave. (Notably, passing the
   //function itself as a value of this is bad: if the function that is being
   //called assumes this to be window, we'll clobber the function value instead.
   //Also, alert won't work if we pass prim as the first argument.)
   return prim.apply(this, argument_array);
}

function to_lower_case(string) {
    return String.prototype.toLowerCase.call(string); // casting for non-strings
}

function to_upper_case(string) {
    return String.prototype.toUpperCase.call(string); // casting for non-strings
}
