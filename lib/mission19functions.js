function test_task(player) {
	CONF_MAX_INERTIA = 4;
	var engine = new DeathCubeEngine(CONT_MODE, LAYOUT19);
	engine.__registerPlayer(player);

	if (is_instance_of(player, ALPHAPlayer)){
		player.take(list(MakeAndInstallHammer(player.getLocation(), 30, COL_RED),
					MakeAndInstallHammer(player.getLocation(), 30, COL_RED),
					MakeAndInstallHammer(player.getLocation(), 30, COL_RED),
					MakeAndInstallALPHATransporter(player.getLocation()))); 
	} else if (is_instance_of(player, BETAPlayer)) {
		player.take(list(MakeAndInstallGrenade(player.getLocation(), 30, COL_ORANGE),
					MakeAndInstallGrenade(player.getLocation(), 30, COL_ORANGE),
					MakeAndInstallGrenade(player.getLocation(), 30, COL_ORANGE),
					MakeAndInstallBETATransporter(player.getLocation()))); 
	} else if (is_instance_of(player, OMEGAPlayer)) {
		player.take(list(MakeAndInstallGun(player.getLocation(), 30, COL_ORANGE),
					MakeAndInstallGun(player.getLocation(), 30, COL_ORANGE),
					MakeAndInstallGun(player.getLocation(), 30, COL_ORANGE),
					MakeAndInstallOMEGATransporter(player.getLocation()))); 
	}

	engine.__addEndGame(
		new EndGame(
			function(){
				var playerItems = player.getPossessions();
				return (!is_empty_list(filter(function(x) { return is_instance_of(x, Flag); }, playerItems)));
			},
			function(){
				alert("Congratulations! You have obtained the flag!");
			}
		)
	);
	engine.__start();
	return engine;
}
