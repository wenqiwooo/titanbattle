// File: mission_19_1.js
	        
/*
copy your class from mission 18-2 here, and extend its functionality
*/

function P1(name) {
	ALPHAPlayer.call(this, name, "Red", "Red", "Red");
	this.previous_rooms = [];
}
P1.Inherits(ALPHAPlayer);

P1.prototype.__act = function() {
	Player.prototype.__act.call(this);
	
    // Get current location
    var cur_loc = this.getLocation();
    
    // Add new visited room to List of visited rooms
    this.previous_rooms = append(this.previous_rooms, list(cur_loc));
    
    // Grab all floor items
    var floor_items = cur_loc.getThings();
    this.take(floor_items);

    // Get list of adjacent rooms
    var adj_rooms = cur_loc.getNeighbours();

    // Number of adjacent rooms to go to
    var len = length(adj_rooms);

    // Adjacent enemies
    var adj_occ = [];
    for (var i = 0; i < len; i = i + 1) {
        adj_occ = append(adj_occ, list_ref(adj_rooms, i).getOccupants());
    }

    // Adjacent enemy location
    var adj_loc = cur_loc;
    if (!is_empty_list(adj_occ)) {
        adj_loc = head(adj_occ).getLocation();
    } else {}

    // Inventory
    var inv = this.getPossessions();

    // Transporter
    var transport = head(filter(function(x) { return is_instance_of(x, Transporter); }, inv));

    // Hammer
    var hammer_list = filter(function(x) { return is_instance_of(x, MeleeWeapon); }, inv);

    // Check if has hammer
    if (!is_empty_list(hammer_list)) {
        var hammer = head(hammer_list);
        
        // Attack enemies
        this.use(hammer, adj_occ);
    } else {}

    // If killed enemy, go take his stuff
    if (adj_loc !== cur_loc) {
        this.use(transport, list(adj_loc));
        this.take(adj_loc.getThings());
    } else {

        while (!transport.isCharging()) {

            // Current position
            var cur_pos = this.getLocation();

            // Adjacent positions
            var adj_rooms = cur_pos.getNeighbours();

            // Define the length of adj_rooms
            var len = length(adj_rooms);

            // Random decision
            var random_decision = Math.floor(Math.random() * len);

            // Choose random room
            var chosen_room = list_ref(adj_rooms, random_decision);

            // Go there
            this.use(transport, list(chosen_room));
        }
    }
};


function P2(name) {
    BETAPlayer.call(this, name, "Orange", "Orange", "Orange");
    this.previous_rooms = [];
}
P2.Inherits(BETAPlayer);

P2.prototype.__act = function() {
    Player.prototype.__act.call(this);
    
    // Get current location
    var cur_loc = this.getLocation();
    
    // Add new visited room to List of visited rooms
    this.previous_rooms = append(this.previous_rooms, list(cur_loc));
    
    // Grab all floor items
    var floor_items = cur_loc.getThings();
    this.take(floor_items);

    // Get list of adjacent rooms
    var adj_rooms = cur_loc.getNeighbours();

    // Number of adjacent rooms to go to
    var len = length(adj_rooms);

    // Adjacent enemies
    var adj_occ = [];
    for (var i = 0; i < len; i = i + 1) {
        adj_occ = append(adj_occ, list_ref(adj_rooms, i).getOccupants());
    }

    // Adjacent enemy location
    var adj_loc = cur_loc;
    if (!is_empty_list(adj_occ)) {
        adj_loc = head(adj_occ).getLocation();
    } else {}

    // Inventory
    var inv = this.getPossessions();

    // Transporter
    var transport = head(filter(function(x) { return is_instance_of(x, Transporter); }, inv));

    // Grenade
    var grenade_list = filter(function(x) { return is_instance_of(x, Bomb); }, inv);

    // Check if has grenade
    if (!is_empty_list(grenade_list)) {
        var grenade = head(grenade_list);
        
        // Attack enemies
        this.use(grenade);
    } else {}

    // If killed enemy, go take his stuff
    if (adj_loc !== cur_loc) {
        this.use(transport, list(adj_loc));
        this.take(adj_loc.getThings());
    } else {

        while (!transport.isCharging()) {

            // Current position
            var cur_pos = this.getLocation();

            // Adjacent positions
            var adj_rooms = cur_pos.getNeighbours();

            // Define the length of adj_rooms
            var len = length(adj_rooms);

            // Random decision
            var random_decision = Math.floor(Math.random() * len);

            // Choose random room
            var chosen_room = list_ref(adj_rooms, random_decision);

            // Go there
            this.use(transport, list(chosen_room));
        }
    }
};


function P3(name) {
    OMEGAPlayer.call(this, name, "Blue", "Blue", "Blue");
    this.previous_rooms = [];
}
P3.Inherits(OMEGAPlayer);

P3.prototype.__act = function() {
    Player.prototype.__act.call(this);
    
    // Get current location
    var cur_loc = this.getLocation();
    
    // Add new visited room to List of visited rooms
    this.previous_rooms = append(this.previous_rooms, list(cur_loc));
    
    // Grab all floor items
    var floor_items = cur_loc.getThings();
    this.take(floor_items);

    // Get list of adjacent rooms
    var adj_rooms = cur_loc.getNeighbours();

    // Number of adjacent rooms to go to
    var len = length(adj_rooms);

    // Adjacent enemies
    var adj_occ = [];
    for (var i = 0; i < len; i = i + 1) {
        adj_occ = append(adj_occ, list_ref(adj_rooms, i).getOccupants());
    }

    // Adjacent enemy location
    var adj_loc = cur_loc;
    if (!is_empty_list(adj_occ)) {
        adj_loc = head(adj_occ).getLocation();
    } else {}

    // Inventory
    var inv = this.getPossessions();

    // Transporter
    var transport = head(filter(function(x) { return is_instance_of(x, Transporter); }, inv));

    // Guns
    var gun_list = filter(function(x) { return is_instance_of(x, RangedWeapon); }, inv);

    // Check if has guns
    if (!is_empty_list(gun_list)) {
        var gun = head(gun_list);
        
        // Attack enemies
        this.use(gun, adj_occ);
    } else {}

    // If killed enemy, go take his stuff
    if (adj_loc !== cur_loc) {
        this.use(transport, list(adj_loc));
        this.take(adj_loc.getThings());
    } else {

        while (!transport.isCharging()) {

            // Current position
            var cur_pos = this.getLocation();

            // Adjacent positions
            var adj_rooms = cur_pos.getNeighbours();

            // Define the length of adj_rooms
            var len = length(adj_rooms);

            // Random decision
            var random_decision = Math.floor(Math.random() * len);

            // Choose random room
            var chosen_room = list_ref(adj_rooms, random_decision);

            // Go there
            this.use(transport, list(chosen_room));
        }
    }
};


// Uncomment the following to test
var newPlayer = new P2("BTA");
//test_task_ALPHA(newPlayer);
test_task(newPlayer);

